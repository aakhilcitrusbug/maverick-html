$(document).ready(function(){ 
    $('#client-owl').owlCarousel({
        loop:true,
        nav:false,
        dots: false,
        stagePadding: 0,
        smartSpeed:500,
        autoplay:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:3,
                dots: true
            },
            600:{
                items:8
            },
            1200:{
                items:8
            }
        }
    });
});
